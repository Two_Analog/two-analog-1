<!DOCTYPE html>
<html>
<head>
	<title>Two Analog</title>
	<!-- {!!HTML::style('front/css/style.css')!!} -->
	<!-- <link rel="stylesheet" type="text/css" href="../front/css/style.css"> -->
	<link rel="stylesheet" type="text/css" href="{{url('front/css/style.css')}}">
	<!-- <link media="all" type="text/css" rel="stylesheet" href="{{url('front/css/style.css'}}"> -->
	<link rel="stylesheet" type="text/css" href="../front/css/bootstrap.css">
    @yield('additional_css')
</head>
<body>
	@include('front.template.header')

	@yield('content')

	@include('front.template.footer')
</body>
</html>